import org.jetbrains.changelog.Changelog
import org.jetbrains.changelog.ChangelogPluginExtension
import org.jlleitschuh.gradle.ktlint.reporter.ReporterType

plugins {
    id("java")
    id("org.jetbrains.kotlin.jvm") version "1.8.21"
    id("org.jetbrains.intellij") version "1.17.3"
    id("org.jetbrains.changelog") version "2.2.0"
    id("org.jlleitschuh.gradle.ktlint") version "11.3.2"
    id("com.javiersc.semver.gradle.plugin") version "0.4.0-alpha.1"
}

group = "jp.masakura.intellij.textlint"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.mockito.kotlin:mockito-kotlin:5.+")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.2")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.9.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.9.2")
}

// Configure Gradle IntelliJ Plugin
// Read more: https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html
intellij {
    version.set("2022.2.5")
    type.set("IC") // Target IDE Platform

    plugins.set(listOf("org.intellij.plugins.markdown"))
}

configure<org.jlleitschuh.gradle.ktlint.KtlintExtension> {
    reporters {
        reporter(ReporterType.PLAIN)
        reporter(ReporterType.SARIF)
    }
}

tasks {
    // Set the JVM compatibility versions
    withType<JavaCompile> {
        sourceCompatibility = "17"
        targetCompatibility = "17"
    }
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions.jvmTarget = "17"
    }

    patchPluginXml {
        sinceBuild.set("222")
        untilBuild.set("243.*")

        val note = getChangeNote(project.changelog, version.get())
        if (note != null) {
            changeNotes.set(note)
            println(changeNotes.get())
        }
    }

    signPlugin {
        certificateChain.set(System.getenv("CERTIFICATE_CHAIN"))
        privateKey.set(System.getenv("PRIVATE_KEY"))
        password.set(System.getenv("PRIVATE_KEY_PASSWORD"))
    }

    publishPlugin {
        token.set(System.getenv("PUBLISH_TOKEN"))
        channels.add(getReleaseChannel(version))
    }

    test {
        useJUnitPlatform()
    }
}

tasks.register("printReleaseChannel") {
    doLast {
        println("Release Channel: ${getReleaseChannel(version)}")
    }
}

tasks.named("publishPlugin") {
    dependsOn("printReleaseChannel")
}

fun getReleaseChannel(version: Any): String {
    val regex = """^(\d+\.\d+\.\d+)$""".toRegex()
    if (regex.matches(version.toString())) return "Stable"
    return "Beta"
}

fun getChangeNote(changelog: ChangelogPluginExtension, version: String): String? {
    if (!changelog.has(version)) return null

    return changelog.renderItem(
        changelog.get(version).withHeader(false).withEmptySections(false),
        Changelog.OutputType.HTML,
    )
}
