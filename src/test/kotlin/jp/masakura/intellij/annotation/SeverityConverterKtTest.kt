package jp.masakura.intellij.annotation

import com.intellij.lang.annotation.HighlightSeverity
import jp.masakura.intellij.textlint.toHighlightSeverity
import jp.masakura.textlint.report.SeverityLevel
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SeverityConverterKtTest {
    @Test
    fun testInformation() {
        assertEquals(HighlightSeverity.INFORMATION, SeverityLevel.Information.toHighlightSeverity())
    }

    @Test
    fun testWarning() {
        assertEquals(HighlightSeverity.WARNING, SeverityLevel.Warning.toHighlightSeverity())
    }

    @Test
    fun testError() {
        assertEquals(HighlightSeverity.ERROR, SeverityLevel.Error.toHighlightSeverity())
    }
}
