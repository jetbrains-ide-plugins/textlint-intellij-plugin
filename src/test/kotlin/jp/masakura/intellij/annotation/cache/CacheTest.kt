package jp.masakura.intellij.annotation.cache

import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.fileTypes.FileTypes
import com.intellij.openapi.fileTypes.LanguageFileType
import com.intellij.openapi.util.TextRange
import jp.masakura.intellij.annotation.issue.Annotation
import jp.masakura.intellij.annotation.issue.Annotations
import jp.masakura.intellij.annotation.issue.Issue
import jp.masakura.intellij.annotation.target.Target
import jp.masakura.testing.project.FakeProject
import jp.masakura.textlint.process.settings.TextlintSettings
import org.intellij.plugins.markdown.lang.MarkdownFileType
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import org.mockito.verification.VerificationMode

class CacheTest {
    private lateinit var target: Cache
    private lateinit var annotate: (Target) -> Annotations
    private lateinit var project1: DummyProject
    private lateinit var project2: DummyProject
    private lateinit var fixture1: Fixture
    private lateinit var fixture2: Fixture

    @BeforeEach
    fun setUp() {
        target = Cache()
        annotate = mock<(Target) -> Annotations>()

        project1 = DummyProject("/project1")
        project2 = DummyProject("/project2")

        fixture1 = project1.fixture("/project1/file1.txt", FileTypes.PLAIN_TEXT)
        fixture2 = project1.fixture("/project1/file2.txt", FileTypes.PLAIN_TEXT)
    }

    @Test
    fun `first access does not use cache`() {
        fixture1.addToMock(annotate)

        whenever(annotate(fixture1.target)).thenReturn(fixture1.annotations)

        val actual = target.getOrInvoke(fixture1.target, annotate)

        assertEquals(fixture1.annotations, actual)
        fixture1.verifyMock(annotate, times(1))
    }

    @Test
    fun `second access does use cache`() {
        fixture1.addToMock(annotate)

        target.getOrInvoke(fixture1.target, annotate)
        val actual = target.getOrInvoke(fixture1.target, annotate)

        assertEquals(fixture1.annotations, actual)
        fixture1.verifyMock(annotate, times(1))
    }

    @Test
    fun `cache is per-file basis`() {
        fixture1.addToMock(annotate)
        fixture2.addToMock(annotate)

        target.getOrInvoke(fixture1.target, annotate)
        val actual = target.getOrInvoke(fixture2.target, annotate)

        assertEquals(fixture2.annotations, actual)
    }

    @Test
    fun `cache is per-project basis`() {
        val p1f1 = project1.fixture("/project1/file1.txt", FileTypes.PLAIN_TEXT)
        p1f1.addToMock(annotate)
        val p2f1 = project2.fixture("/project2/file1.txt", FileTypes.PLAIN_TEXT)
        p2f1.addToMock(annotate)

        target.getOrInvoke(p1f1.target, annotate)
        target.getOrInvoke(p2f1.target, annotate)

        p2f1.verifyMock(annotate, times(1))
    }

    @Test
    fun `cache is per-filetype basis`() {
        val markdown = project1.fixture("/project1/file.md", MarkdownFileType.INSTANCE)
        markdown.addToMock(annotate)
        val plaintext = project1.fixture("/project1/file.md", FileTypes.PLAIN_TEXT)
        plaintext.addToMock(annotate)

        target.getOrInvoke(markdown.target, annotate)
        target.getOrInvoke(plaintext.target, annotate)

        plaintext.verifyMock(annotate, times(1))
    }

    @Test
    fun `discard cache if the file is updated`() {
        val revision1 = fixture1.modify()
        revision1.addToMock(annotate)
        val revision2 = revision1.modify()
        revision2.addToMock(annotate)

        target.getOrInvoke(revision1.target, annotate)
        target.getOrInvoke(revision2.target, annotate)

        revision2.verifyMock(annotate, times(1))
    }
}

private class DummyProject(private val path: String) {
    private val project = FakeProject.create(path, TextlintSettings.DEFAULT)

    fun fixture(
        filePath: String,
        fileType: LanguageFileType,
    ): Fixture {
        return Fixture(filePath, fileType, 0, path, project)
    }
}

private class Fixture(
    private val path: String,
    private val fileType: LanguageFileType,
    private val modificationCount: Long,
    private val projectPath: String,
    private val project: FakeProject,
) {
    fun addToMock(annotate: (Target) -> Annotations) {
        whenever(annotate(target)).thenReturn(annotations)
    }

    fun verifyMock(
        annotate: (Target) -> Annotations,
        times: VerificationMode,
    ) {
        verify(annotate, times).invoke(target)
    }

    fun modify(): Fixture {
        return Fixture(path, fileType, modificationCount + 1, projectPath, project)
    }

    val content = "$path:${fileType.name}:$modificationCount"
    val target = project.target(path, content, fileType, modificationCount)
    val annotations =
        Annotations(
            listOf(
                Annotation(
                    Issue(content, HighlightSeverity.INFORMATION, "message", TextRange(0, 1)),
                ),
            ),
        )
}
