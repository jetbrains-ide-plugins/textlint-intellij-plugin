package jp.masakura.intellij.textlint

import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.util.TextRange
import jp.masakura.intellij.annotation.issue.Annotation
import jp.masakura.intellij.annotation.issue.Annotations

data class IssueResult(
    val severity: HighlightSeverity,
    val message: String,
    val range: TextRange,
) {
    companion object {
        fun from(annotation: Annotation): IssueResult {
            return IssueResult(
                annotation.issue.severity,
                annotation.issue.message.toString(),
                annotation.issue.range,
            )
        }
    }
}

fun Annotations.toIssues(): List<IssueResult> {
    return this.map { IssueResult.from(it) }
}
