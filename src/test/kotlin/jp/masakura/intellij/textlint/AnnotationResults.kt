package jp.masakura.intellij.textlint

import com.intellij.codeInsight.daemon.impl.AnnotationHolderImpl

class AnnotationResults(private val annotations: List<AnnotationResult>) {
    fun quickFixes(): IntentionActions {
        return IntentionActions.from(annotations)
    }

    fun issues(): List<IssueResult> {
        val list = mutableListOf<IssueResult>()

        annotations.forEach {
            list.add(it.issue)
        }

        return list.toList()
    }

    fun first(): AnnotationResult {
        return annotations[0]
    }

    companion object {
        fun from(annotationHolder: AnnotationHolderImpl): AnnotationResults {
            return AnnotationResults(annotationHolder.map { AnnotationResult.from(it) })
        }
    }
}
