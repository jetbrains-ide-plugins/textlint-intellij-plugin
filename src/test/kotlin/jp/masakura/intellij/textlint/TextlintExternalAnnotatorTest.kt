package jp.masakura.intellij.textlint

import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiFile
import com.intellij.testFramework.TestApplicationManager
import com.intellij.testFramework.junit5.EdtInterceptor
import jp.masakura.testing.file.FakeTextlintFactory
import jp.masakura.testing.fixture.Fixtures
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(EdtInterceptor::class)
class TextlintExternalAnnotatorTest {
    private lateinit var target: ExternalAnnotatorAdapter
    private lateinit var myProject: ProjectAdapter

    @BeforeEach
    fun setUp() {
        myProject = ProjectAdapter.load("test-project")

        val factory = FakeTextlintFactory()
            .printedConfig(Fixtures.printedConfigs.DEFAULT)
            .add(Fixtures.file)
            .add(Fixtures.issue)
            .add(Fixtures.noIssue)
            .add(Fixtures.fix)

        val annotator = TextlintExternalAnnotator(factory)
        target = ExternalAnnotatorAdapter(annotator)
    }

    @AfterEach
    fun tearDown() {
        myProject.close()
    }

    @Test
    fun testAnnotation() {
        val fixture = Fixtures.issue
        val file = myProject.openFile(fixture.file)

        val actual = target.perform(file).issues()

        assertEquals(fixture.annotations.appendLabel(TextlintLabel.instance).toIssues(), actual)
    }

    @Test
    fun testNoAnnotation() {
        val file = myProject.openFile(Fixtures.noIssue.file)

        val actual = target.perform(file).issues()

        assertEquals(emptyList<IssueResult>(), actual)
    }

    @Test
    fun testQuickFix() {
        val fixture = Fixtures.fix
        val file = myProject.openFile("tests/quick-fix1.md")
        val expected = fixture.file.content.replace("ポ", "ポ")

        val quickFixes = target.perform(file).first().quickFixes

        quickFixes.first().invoke(file.project, null, file)

        assertEquals(expected, file.documentText())
    }

    @Test
    fun testQuickFixFamilyName() {
        val file = myProject.openFile(Fixtures.fix.file)

        val quickFixes = target.perform(file).quickFixes()

        assertEquals("TEXTLINT", quickFixes.first().familyName)
    }

    @Test
    fun testQuickFixText() {
        val fixture = Fixtures.fix
        val file = myProject.openFile(fixture.file)

        val quickFixes = target.perform(file).first().quickFixes

        assertEquals(
            listOf("textlint: Fix 'no-nfd'", "textlint: Fix current file"),
            quickFixes.texts(),
        )
    }

    @Test
    fun testQuickFixCurrentFile() {
        val file = myProject.openFile("tests/quick-fix2.md")

        val quickFixes = target.perform(file).first().quickFixes

        quickFixes.forCurrentFile().invoke(file.project, null, file)

        assertEquals("ポケット エンジン", file.documentText().trim())
    }

    companion object {
        init {
            TestApplicationManager.getInstance()
        }
    }
}

private fun PsiFile.documentText(): String {
    val document = PsiDocumentManager.getInstance(project).getDocument(this)!!
    return document.text
}
