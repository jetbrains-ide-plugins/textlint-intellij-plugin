package jp.masakura.intellij.textlint

import com.intellij.codeInsight.intention.IntentionAction
import com.intellij.lang.annotation.Annotation
import com.intellij.openapi.util.TextRange
import java.security.InvalidParameterException

data class AnnotationResult(val issue: IssueResult, val quickFixes: IntentionActions) {
    constructor(issue: IssueResult, quickFixes: List<IntentionAction>) : this(issue, IntentionActions(quickFixes))

    companion object {
        fun from(annotation: Annotation?): AnnotationResult {
            if (annotation == null) throw InvalidParameterException()

            return AnnotationResult(
                IssueResult(annotation.severity, annotation.message, TextRange.create(annotation)),
                annotation.quickFixes?.map { it.quickFix } ?: emptyList(),
            )
        }
    }
}
