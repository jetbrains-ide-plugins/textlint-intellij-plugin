package jp.masakura.intellij.textlint.configuration

import jp.masakura.textlint.process.settings.TextlintExcludeFiles
import jp.masakura.textlint.process.settings.TextlintSettings
import jp.masakura.textlint.process.settings.TextlintSettingsFilesToScan
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class PluginSettingsTest {
    @Test
    fun testApply() {
        val source =
            PluginSettingsImpl(
                TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE,
                TextlintSettingsFilesToScan.USE_PRINT_CONFIG,
            )
        val destination =
            PluginSettingsImpl(
                TextlintExcludeFiles.USE_TEXTLINT_IGNORE,
                TextlintSettingsFilesToScan.ANY_FILES,
            )

        source.apply(destination)

        assertEquals(destination, source)
    }

    @Test
    fun modifiedExcludeFiles() {
        val original =
            PluginSettingsImpl(
                TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE,
                TextlintSettingsFilesToScan.USE_PRINT_CONFIG,
            )
        val current =
            PluginSettingsImpl(
                TextlintExcludeFiles.USE_TEXTLINT_IGNORE,
                TextlintSettingsFilesToScan.USE_PRINT_CONFIG,
            )

        assertTrue(current.isModified(original))
    }

    @Test
    fun modifiedPrintConfig() {
        val original =
            PluginSettingsImpl(
                TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE,
                TextlintSettingsFilesToScan.USE_PRINT_CONFIG,
            )
        val current =
            PluginSettingsImpl(
                TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE,
                TextlintSettingsFilesToScan.ANY_FILES,
            )

        assertTrue(current.isModified(original))
    }

    @Test
    fun `not modified`() {
        val original =
            PluginSettingsImpl(
                TextlintExcludeFiles.USE_TEXTLINT_IGNORE,
                TextlintSettingsFilesToScan.USE_PRINT_CONFIG,
            )
        val current =
            PluginSettingsImpl(
                TextlintExcludeFiles.USE_TEXTLINT_IGNORE,
                TextlintSettingsFilesToScan.USE_PRINT_CONFIG,
            )

        assertFalse(current.isModified(original))
    }

    @Test
    fun testToTextlintSettings() {
        val source =
            PluginSettingsImpl(
                TextlintExcludeFiles.USE_TEXTLINT_IGNORE,
                TextlintSettingsFilesToScan.USE_PRINT_CONFIG,
            )

        assertEquals(
            TextlintSettings(
                TextlintExcludeFiles.USE_TEXTLINT_IGNORE,
                TextlintSettingsFilesToScan.USE_PRINT_CONFIG,
            ),
            source.toTextlintSettings(),
        )
    }
}
