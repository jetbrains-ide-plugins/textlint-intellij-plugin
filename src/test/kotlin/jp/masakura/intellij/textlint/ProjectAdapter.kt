package jp.masakura.intellij.textlint

import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiManager
import com.intellij.testFramework.PlatformTestUtil
import com.intellij.testFramework.VfsTestUtil
import jp.masakura.testing.fixture.TestFile
import java.nio.file.Path
import java.nio.file.Paths

class ProjectAdapter(private val project: Project) : AutoCloseable {
    private val psiManager: PsiManager
        get() = PsiManager.getInstance(project)

    fun openFile(relativePath: String): PsiFile {
        val path = Paths.get(project.basePath!!, relativePath)
        val file = VfsTestUtil.findFileByCaseSensitivePath(path.toString())
        return psiManager.findFile(file)!!
    }

    fun openFile(file: TestFile): PsiFile {
        val virtualFile = VfsTestUtil.findFileByCaseSensitivePath(file.path)
        return psiManager.findFile(virtualFile)!!
    }

    override fun close() {
        PlatformTestUtil.forceCloseProjectWithoutSaving(project)
    }

    companion object {
        fun load(relativePath: String): ProjectAdapter {
            val path = Path.of(System.getProperty("user.dir")).resolve(Path.of(relativePath))
            return ProjectAdapter(PlatformTestUtil.loadAndOpenProject(path) {})
        }
    }
}
