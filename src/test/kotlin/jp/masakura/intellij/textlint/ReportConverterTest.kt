package jp.masakura.intellij.textlint

import jp.masakura.testing.fixture.Fixtures
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ReportConverterTest {
    @Test
    fun testToAnnotations() {
        val file = Fixtures.file

        val result = file.textlint.report.toAnnotations()

        assertEquals(file.annotations, result)
    }
}
