package jp.masakura.testing.fixture

import jp.masakura.intellij.annotation.issue.Annotation
import jp.masakura.intellij.annotation.issue.Annotations

data class Fixture(
    val file: TestFile,
    val textlint: TextlintFixture,
    val annotations: Annotations,
) {
    constructor(file: TestFile, textlint: TextlintFixture, annotations: List<Annotation>) :
        this(file, textlint, Annotations(annotations))
}
