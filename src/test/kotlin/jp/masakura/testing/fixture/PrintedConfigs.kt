package jp.masakura.testing.fixture

object PrintedConfigs {
    val EMPTY = "{}"
    val DEFAULT =
        """
        {
            "plugin": [
                {"id": "@textlint/textlint-plugin-text"},
                {"id": "@textlint/textlint-plugin-markdown"}
            ]
        }
        """.trimIndent()
    val HTML =
        """
        {
            "plugin": [
                {"id": "@textlint/textlint-plugin-text"},
                {"id": "@textlint/textlint-plugin-markdown"},
                {"id": "html"}
            ]
        }
        """.trimIndent()
    val RST: String =
        """
        {
            "plugin": [
                {"id": "@textlint/textlint-plugin-text"},
                {"id": "@textlint/textlint-plugin-markdown"},
                {"id": "rst"}
            ]
        }
        """.trimIndent()
}
