package jp.masakura.testing.fixture

import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.util.TextRange
import jp.masakura.intellij.annotation.issue.Annotation
import jp.masakura.intellij.annotation.quickfix.FixPart
import jp.masakura.intellij.annotation.quickfix.QuickFix
import jp.masakura.textlint.report.* // ktlint-disable no-wildcard-imports

object Fixtures {
    val file = Fixture(
        TestFile.load("test-project/directory/file.md"),
        TextlintFixture(
            """
            [{"messages":[{"type":"lint","ruleId":"no-todo","message":"Found TODO: '- [ ] write usage instruction'","index":11,"line":2,"column":3,"range":[11,12],"loc":{"start":{"line":2,"column":3},"end":{"line":2,"column":4}},"severity":2},{"type":"lint","ruleId":"no-nfd","message":"Disallow to use NFD(well-known as UTF8-MAC 濁点): \"ホ゜\" => \"ポ\"","index":42,"line":3,"column":4,"range":[42,43],"loc":{"start":{"line":3,"column":4},"end":{"line":3,"column":5}},"severity":2,"fix":{"range":[41,43],"text":"ポ"}},{"type":"lint","ruleId":"no-nfd","message":"Disallow to use NFD(well-known as UTF8-MAC 濁点): \"シ゛\" => \"ジ\"","index":50,"line":3,"column":12,"range":[50,51],"loc":{"start":{"line":3,"column":12},"end":{"line":3,"column":13}},"severity":2,"fix":{"range":[49,51],"text":"ジ"}}],"filePath":"unused"}]
            """.trimIndent(),
            TextlintReport(
                listOf(
                    Issue(
                        listOf(
                            Message(
                                "lint",
                                "no-todo",
                                "Found TODO: '- [ ] write usage instruction'",
                                11,
                                2,
                                3,
                                listOf(11, 12),
                                Location(Position(2, 3), Position(2, 4)),
                                SeverityLevel.Error,
                            ),
                            Message(
                                "lint",
                                "no-nfd",
                                "Disallow to use NFD(well-known as UTF8-MAC 濁点): \"ホ゜\" => \"ポ\"",
                                42,
                                3,
                                4,
                                listOf(42, 43),
                                Location(Position(3, 4), Position(3, 5)),
                                SeverityLevel.Error,
                                Fix(listOf(41, 43), "ポ"),
                            ),
                            Message(
                                "lint",
                                "no-nfd",
                                "Disallow to use NFD(well-known as UTF8-MAC 濁点): \"シ゛\" => \"ジ\"",
                                50,
                                3,
                                12,
                                listOf(50, 51),
                                Location(Position(3, 12), Position(3, 13)),
                                SeverityLevel.Error,
                                Fix(listOf(49, 51), "ジ"),
                            ),
                        ),
                        "unused",
                    ),
                ),
            ),
        ),
        listOf(
            Annotation(
                jp.masakura.intellij.annotation.issue.Issue(
                    "no-todo",
                    HighlightSeverity.ERROR,
                    "Found TODO: '- [ ] write usage instruction' (no-todo)",
                    TextRange(11, 12),
                ),
            ),
            Annotation(
                jp.masakura.intellij.annotation.issue.Issue(
                    "no-nfd",
                    HighlightSeverity.ERROR,
                    "Disallow to use NFD(well-known as UTF8-MAC 濁点): \"ホ゜\" => \"ポ\" (no-nfd)",
                    TextRange(42, 43),
                ),
                listOf(
                    QuickFix(FixPart("ポ", 41, 43), "Fix 'no-nfd'"),
                ),
            ),
            Annotation(
                jp.masakura.intellij.annotation.issue.Issue(
                    "no-nfd",
                    HighlightSeverity.ERROR,
                    "Disallow to use NFD(well-known as UTF8-MAC 濁点): \"シ゛\" => \"ジ\" (no-nfd)",
                    TextRange(50, 51),
                ),
                listOf(
                    QuickFix(FixPart("ジ", 49, 51), "Fix 'no-nfd'"),
                ),
            ),
        ),
    )

    val issue = Fixture(
        TestFile.load("test-project/tests/issue.md"),
        TextlintFixture(
            """
            [{"messages":[{"type":"lint","ruleId":"no-todo","message":"Found TODO: '- [ ] task1'","index":2,"line":1,"column":3,"range":[2,3],"loc":{"start":{"line":1,"column":3},"end":{"line":1,"column":4}},"severity":2}],"filePath":"unused"}]
            """.trimIndent(),
            TextlintReport(
                listOf(
                    Issue(
                        listOf(
                            Message(
                                "lint",
                                "no-todo",
                                "Found TODO: '- [ ] task1'",
                                2,
                                1,
                                3,
                                listOf(2, 3),
                                Location(Position(1, 3), Position(1, 4)),
                                SeverityLevel.Error,
                            ),
                        ),
                        "unused",
                    ),
                ),
            ),
        ),
        listOf(
            Annotation(
                jp.masakura.intellij.annotation.issue.Issue(
                    "no-todo",
                    HighlightSeverity.ERROR,
                    "Found TODO: '- [ ] task1' (no-todo)",
                    TextRange(2, 3),
                ),
            ),
        ),
    )

    val noIssue = Fixture(
        TestFile.load("test-project/tests/no-issue.md"),
        TextlintFixture(
            """
            [{"messages":[],"filePath":"unused"}]
            """.trimIndent(),
            TextlintReport(
                listOf(
                    Issue(
                        emptyList(),
                        "unused",
                    ),
                ),
            ),
        ),
        emptyList(),
    )

    val fix = Fixture(
        TestFile.load("test-project/tests/quick-fix.md"),
        TextlintFixture(
            """
            [{"messages":[{"type":"lint","ruleId":"no-nfd","message":"Disallow to use NFD(well-known as UTF8-MAC 濁点): \"ホ゜\" => \"ポ\"","index":1,"line":1,"column":2,"range":[1,2],"loc":{"start":{"line":1,"column":2},"end":{"line":1,"column":3}},"severity":2,"fix":{"range":[0,2],"text":"ポ"}},{"type":"lint","ruleId":"no-nfd","message":"Disallow to use NFD(well-known as UTF8-MAC 濁点): \"シ゛\" => \"ジ\"","index":9,"line":1,"column":10,"range":[9,10],"loc":{"start":{"line":1,"column":10},"end":{"line":1,"column":11}},"severity":2,"fix":{"range":[8,10],"text":"ジ"}}],"filePath":"unused"}]
            """.trimIndent(),
            TextlintReport(
                listOf(
                    Issue(
                        listOf(
                            Message(
                                "lint",
                                "no-nfd",
                                "Disallow to use NFD(well-known as UTF8-MAC 濁点): \"ホ゜\" => \"ポ\"",
                                1,
                                1,
                                2,
                                listOf(1, 2),
                                Location(Position(1, 2), Position(1, 3)),
                                SeverityLevel.Error,
                                Fix(listOf(0, 2), "ポ"),
                            ),
                            Message(
                                "lint",
                                "no-nfd",
                                "Disallow to use NFD(well-known as UTF8-MAC 濁点): \"シ゛\" => \"ジ\"",
                                9,
                                1,
                                10,
                                listOf(9, 10),
                                Location(Position(1, 10), Position(1, 10)),
                                SeverityLevel.Error,
                                Fix(listOf(8, 10), "ジ"),
                            ),
                        ),
                        "unused",
                    ),
                ),
            ),
        ),
        listOf(
            Annotation(
                jp.masakura.intellij.annotation.issue.Issue(
                    "no-nfd",
                    HighlightSeverity.ERROR,
                    "Disallow to use NFD(well-known as UTF8-MAC 濁点): \"ホ゜\" => \"ポ\" (no-nfd)",
                    TextRange(1, 2),
                ),
                listOf(
                    QuickFix(FixPart("ポ", 0, 2), "Fix 'no-nfd'"),
                ),
            ),
            Annotation(
                jp.masakura.intellij.annotation.issue.Issue(
                    "no-nfd",
                    HighlightSeverity.ERROR,
                    "Disallow to use NFD(well-known as UTF8-MAC 濁点): \"シ゛\" => \"ジ\" (no-nfd)",
                    TextRange(9, 10),
                ),
                listOf(
                    QuickFix(FixPart("ジ", 8, 10), "Fix 'no-nfd'"),
                ),
            ),
        ),
    )

    val printedConfigs = PrintedConfigs
}
