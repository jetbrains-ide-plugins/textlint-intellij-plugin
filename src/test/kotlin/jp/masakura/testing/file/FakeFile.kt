package jp.masakura.testing.file

import java.nio.file.Paths

class FakeFile(private val path: String, val content: String) {
    fun toAbsolutePathFile(workDirectory: String): FakeFile {
        return FakeFile(Paths.get(workDirectory, path).toString(), content)
    }

    fun equalsPath(absolutePath: String): Boolean {
        return path == absolutePath
    }

    companion object {
        fun empty(relativePath: String): FakeFile {
            return FakeFile(relativePath, "")
        }
    }
}
