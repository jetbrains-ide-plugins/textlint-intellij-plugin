package jp.masakura.testing.file

import jp.masakura.textlint.file.FileSystem

class FakeFileSystem(private val files: List<FakeFile>) : FileSystem {

    override fun exists(absolutePath: String): Boolean {
        return files.any { it.equalsPath(absolutePath) }
    }

    override fun readText(absolutePath: String): String {
        val file = files.first { it.equalsPath(absolutePath) }
        return file.content
    }
}
