package jp.masakura.testing.project

import com.intellij.openapi.vfs.VirtualFile
import com.intellij.openapi.vfs.newvfs.impl.FakeVirtualFile
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import java.nio.file.Paths

abstract class FakeVirtualFile : VirtualFile() {
    companion object {

        fun create(
            basePath: String,
            relativePath: String,
            myModificationCount: Long,
        ): FakeVirtualFile {
            val fullPath = Paths.get(basePath, relativePath).toString()
            return mock<FakeVirtualFile> {
                on { path } doReturn fullPath
                on { modificationCount } doReturn myModificationCount
            }
        }
    }
}
