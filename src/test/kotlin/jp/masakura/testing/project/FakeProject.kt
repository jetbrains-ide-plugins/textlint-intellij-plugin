package jp.masakura.testing.project

import com.intellij.openapi.fileTypes.FileType
import com.intellij.openapi.fileTypes.FileTypes
import com.intellij.openapi.project.Project
import jp.masakura.intellij.annotation.target.Target
import jp.masakura.intellij.textlint.configuration.PluginSettingsState
import jp.masakura.textlint.process.settings.TextlintSettings
import org.mockito.Mockito
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import java.nio.file.Path
import java.nio.file.Paths

abstract class FakeProject : Project {
    abstract val projectDir: String

    fun file(
        relativePath: String,
        content: String,
        fileType: FileType = FileTypes.UNKNOWN,
        modificationCount: Long = 0,
    ): FakePsiFile {
        return FakePsiFile.create(
            this,
            relativePath,
            content,
            fileType,
            modificationCount,
        )
    }

    fun target(
        relativePath: String,
        content: String,
        fileType: FileType = FileTypes.UNKNOWN,
        modificationCount: Long = 0,
    ): Target {
        return Target.from(file(relativePath, content, fileType, modificationCount))
    }

    companion object {
        fun create(
            myProjectDir: String,
            state: PluginSettingsState,
        ): FakeProject {
            val path = absolutePath(myProjectDir)
            return mock<FakeProject>(defaultAnswer = Mockito.CALLS_REAL_METHODS) {
                on { basePath } doReturn path
                on { projectDir } doReturn path
                on { getService(PluginSettingsState::class.java) } doReturn state
            }
        }

        fun create(
            myProjectDir: String,
            settings: TextlintSettings,
        ): FakeProject {
            return create(myProjectDir, PluginSettingsState.from(settings))
        }

        private fun absolutePath(dir: String): String {
            if (Path.of(dir).isAbsolute) return dir
            return Paths.get(System.getProperty("user.dir"), dir).toString()
        }
    }
}
