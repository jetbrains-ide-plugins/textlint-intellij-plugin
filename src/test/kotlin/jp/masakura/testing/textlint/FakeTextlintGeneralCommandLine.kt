package jp.masakura.testing.textlint

import com.intellij.execution.configurations.GeneralCommandLine
import jp.masakura.testing.fixture.Fixture
import jp.masakura.textlint.command.Npx

class FakeTextlintGeneralCommandLine(
    private val fixtures: List<Fixture>,
    private val textlintrc: String,
) : GeneralCommandLine() {
    override fun createProcess(): Process {
        if (exePath != Npx.DEFAULT.command) throw IllegalStateException("require exePath is npx")
        if (parametersList[0] != "textlint") throw IllegalStateException("require textlint command")

        val options = TextlintCommandLineOptions.parse(parametersList.parameters)

        return FakeTextlintFixProcess.create(options)
            ?: FakeTextlintPrintedConfigProcess.create(options, textlintrc)
            ?: FakeTextlintScanProcess.create(options, fixtures)
    }
}
