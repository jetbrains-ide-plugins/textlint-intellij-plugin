package jp.masakura.testing.textlint

import org.junit.jupiter.api.Assertions.* // ktlint-disable no-wildcard-imports
import org.junit.jupiter.api.Test

class TextlintCommandLineOptionsTest {
    @Test
    fun `--format json`() {
        val options = TextlintCommandLineOptions.parse(listOf("--format", "json"))

        assertEquals("json", options.format)
    }

    @Test
    fun `no --format`() {
        val options = TextlintCommandLineOptions.parse(listOf("--dummy"))

        assertNull(options.format)
    }

    @Test
    fun `--stdin`() {
        val options = TextlintCommandLineOptions.parse(listOf("--stdin"))

        assertTrue(options.stdin)
    }

    @Test
    fun `no --stdin`() {
        val options = TextlintCommandLineOptions.parse(listOf("--dummy"))

        assertFalse(options.stdin)
    }

    @Test
    fun `--stdin-filename file_md`() {
        val options = TextlintCommandLineOptions.parse(listOf("--stdin-filename", "file.md"))

        assertEquals("file.md", options.stdinFilename)
    }

    @Test
    fun `no --stdin-filename`() {
        val options = TextlintCommandLineOptions.parse(listOf("--dummy"))

        assertNull(options.stdinFilename)
    }
}
