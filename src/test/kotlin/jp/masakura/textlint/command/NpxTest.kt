package jp.masakura.textlint.command

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class NpxTest {
    @Test
    fun testWindows() {
        val target = Npx.from("Windows 11")

        assertEquals("npx.cmd", target.command)
    }

    @Test
    fun testLinux() {
        val target = Npx.from("Linux")

        assertEquals("npx", target.command)
    }

    @Test
    fun testMac() {
        val target = Npx.from("macOS")

        assertEquals("npx", target.command)
    }
}
