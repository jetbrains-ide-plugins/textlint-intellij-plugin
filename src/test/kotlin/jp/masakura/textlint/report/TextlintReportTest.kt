package jp.masakura.textlint.report

import jp.masakura.testing.fixture.Fixtures
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TextlintReportTest {
    @Test
    fun testFrom() {
        val file = Fixtures.file

        val result = TextlintReport.from(file.textlint.json.byteInputStream())

        assertEquals(file.textlint.report, result)
    }

    @Test
    fun testEmptyString() {
        val actual = TextlintReport.from("".byteInputStream())

        assertEquals(actual.issues.count(), 0)
    }
}
