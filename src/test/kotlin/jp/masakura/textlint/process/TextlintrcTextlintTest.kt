package jp.masakura.textlint.process

import jp.masakura.intellij.file.TargetFile
import jp.masakura.testing.file.FakeFileSystemBuilder
import jp.masakura.textlint.report.* // ktlint-disable no-wildcard-imports
import org.junit.jupiter.api.Assertions.* // ktlint-disable no-wildcard-imports
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

class TextlintrcTextlintTest {
    private lateinit var mockTextlint: Textlint
    private lateinit var file: TargetFile

    @BeforeEach
    fun setUp() {
        file = TargetFile("/project1/sample.md", "ポケット エンジン")
        mockTextlint = mock<Textlint> {
            on { scan(file) } doReturn REPORT
            on { fix(file) } doReturn "ポケットエンジン"
            on { isSupported(file) } doReturn true
        }
    }

    @Test
    fun `pass through scan`() {
        val files = FakeFileSystemBuilder()
            .build("/project1")
        val target = TextlintrcTextlint(mockTextlint, "/project1", files)

        assertEquals(REPORT, target.scan(file))
    }

    @Test
    fun `pass through fix`() {
        val files = FakeFileSystemBuilder()
            .build("/project1")
        val target = TextlintrcTextlint(mockTextlint, "/project1", files)

        assertEquals("ポケットエンジン", target.fix(file))
    }

    @Test
    fun `supported when a textlintrc file exists`() {
        val files = FakeFileSystemBuilder()
            .add(".textlintrc.json")
            .build("/project1")
        val target = TextlintrcTextlint(mockTextlint, "/project1", files)

        assertTrue(target.isSupported(file))
    }

    @Test
    fun `not supported when a textlintrc file not exists`() {
        val files = FakeFileSystemBuilder()
            .build("/project1")
        val target = TextlintrcTextlint(mockTextlint, "/project1", files)

        assertFalse(target.isSupported(file))
    }

    @Test
    fun `recognize the '_textlintrc' file`() {
        val files = FakeFileSystemBuilder()
            .add(".textlintrc")
            .build("/project1")
        val target = TextlintrcTextlint(mockTextlint, "/project1", files)

        assertTrue(target.isSupported(file))
    }

    @Test
    fun `recognize the '_textlintrc_yaml' file`() {
        val files = FakeFileSystemBuilder()
            .add(".textlintrc.yaml")
            .build("/project1")
        val target = TextlintrcTextlint(mockTextlint, "/project1", files)

        assertTrue(target.isSupported(file))
    }

    @Test
    fun `recognize the '_textlintrc_yml' file`() {
        val files = FakeFileSystemBuilder()
            .add(".textlintrc.yml")
            .build("/project1")
        val target = TextlintrcTextlint(mockTextlint, "/project1", files)

        assertTrue(target.isSupported(file))
    }

    @Test
    fun `recognize the '_textlintrc_js' file`() {
        val files = FakeFileSystemBuilder()
            .add(".textlintrc.js")
            .build("/project1")
        val target = TextlintrcTextlint(mockTextlint, "/project1", files)

        assertTrue(target.isSupported(file))
    }

    companion object {
        private val REPORT = TextlintReport(
            listOf(
                Issue(
                    listOf(
                        Message(
                            "type",
                            "rule",
                            "message",
                            0, 0, 0,
                            listOf(0, 0),
                            Location(Position(0, 0), Position(0, 0)),
                            SeverityLevel.Error,
                        ),
                    ),
                    "/project1/sample.md",
                ),
            ),
        )
    }
}
