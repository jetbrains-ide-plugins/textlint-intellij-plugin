package jp.masakura.textlint.process

import jp.masakura.intellij.file.TargetFile
import jp.masakura.testing.file.FakeFile
import jp.masakura.testing.file.FakeFileSystemBuilder
import jp.masakura.textlint.process.settings.TextlintIgnoreTextlint
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import java.nio.file.Paths

class TextlintIgnoreTextlintTest {
    @Test
    fun `not exists textlintignore`() {
        val project = FakeProject.create("/project1")
        val file = project.file("file.txt")
        whenever(project.mockTextlint.isSupported(file)).thenReturn(true)

        val target = project.createTextlintIgnoreTextlint()
        val actual = target.isSupported(file)

        assertTrue(actual)
        verify(project.mockTextlint, times(1)).isSupported(file)
    }

    @Test
    fun `ignore file_txt`() {
        val project =
            FakeProject.create("/project1")
                .textlintIgnore("file.txt")
        val file = project.file("file.txt")

        val target = project.createTextlintIgnoreTextlint()
        val actual = target.isSupported(file)

        assertFalse(actual)
        verify(project.mockTextlint, times(0)).isSupported(file)
    }

    @Test
    fun `not ignore file_txt`() {
        val project =
            FakeProject.create("/project1")
                .textlintIgnore("file.md")
        val file = project.file("file.txt")
        whenever(project.mockTextlint.isSupported(file)).thenReturn(true)

        val target = project.createTextlintIgnoreTextlint()
        val actual = target.isSupported(file)

        assertTrue(actual)
        verify(project.mockTextlint, times(1)).isSupported(file)
    }
}

class FakeProject(
    private val workDirectory: String,
    private val files: FakeFileSystemBuilder,
    val mockTextlint: Textlint,
) {
    fun file(relativePath: String): TargetFile {
        return TargetFile(Paths.get(workDirectory, relativePath).toString(), "")
    }

    fun textlintIgnore(text: String): FakeProject {
        return FakeProject(
            workDirectory,
            files.add(FakeFile(".textlintignore", text)),
            mockTextlint,
        )
    }

    fun createTextlintIgnoreTextlint(): TextlintIgnoreTextlint {
        return TextlintIgnoreTextlint(mockTextlint, workDirectory, files.build(workDirectory))
    }

    companion object {
        fun create(workDirectory: String): FakeProject {
            return FakeProject(
                workDirectory,
                FakeFileSystemBuilder(),
                mock<Textlint>(),
            )
        }
    }
}
