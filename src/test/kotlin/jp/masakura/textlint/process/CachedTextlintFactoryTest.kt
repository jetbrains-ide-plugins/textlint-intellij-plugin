@file:Suppress("ktlint:standard:no-wildcard-imports")

package jp.masakura.textlint.process

import jp.masakura.intellij.file.TargetFile
import jp.masakura.textlint.process.settings.TextlintExcludeFiles
import jp.masakura.textlint.process.settings.TextlintSettings
import jp.masakura.textlint.process.settings.TextlintSettingsFilesToScan
import jp.masakura.textlint.report.TextlintReport
import org.junit.jupiter.api.Assertions.assertNotSame
import org.junit.jupiter.api.Assertions.assertSame
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.mock

class CachedTextlintFactoryTest {
    private lateinit var mockFactory: TextlintFactory
    private lateinit var target: TextlintFactory
    private lateinit var setting2: TextlintSettings
    private lateinit var setting1: TextlintSettings

    @BeforeEach
    fun setUp() {
        setting1 =
            TextlintSettings(
                TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE,
                TextlintSettingsFilesToScan.USE_PRINT_CONFIG,
            )
        setting2 =
            TextlintSettings(
                TextlintExcludeFiles.USE_TEXTLINT_IGNORE,
                TextlintSettingsFilesToScan.USE_PRINT_CONFIG,
            )

        mockFactory =
            mock<TextlintFactory> {
                on { create("/project1", setting1) } doAnswer { MockTextlint("project1-setting1") }
                on { create("/project1", setting2) } doAnswer { MockTextlint("project1-setting2") }
                on { create("/project2", setting1) } doAnswer { MockTextlint("project2-setting1") }
                on { create("/project2", setting2) } doAnswer { MockTextlint("project2-setting2") }
            }
        target = TextlintFactory.withCache(mockFactory)
    }

    @Test
    fun `return cache on the second time`() {
        val first = target.create("/project1", setting1)

        assertSame(first, target.create("/project1", setting1))
    }

    @Test
    fun `cache is at the unit of workDirectory`() {
        val first = target.create("/project1", setting1)

        assertNotSame(first, target.create("/project2", setting1))
    }

    @Test
    fun `cache is not used when settings are changed`() {
        val target = TextlintFactory.withCache(mockFactory)

        val first = target.create("/project1", setting1)

        assertNotSame(first, target.create("/project1", setting2))
    }

    @Test
    fun `cache is discarded when settings are changed`() {
        val first = target.create("/project1", setting1)
        target.create("/project1", setting2)

        assertNotSame(first, target.create("/project1", setting1))
    }
}

private class MockTextlint(private val type: String) : Textlint {
    override fun scan(file: TargetFile): TextlintReport {
        throw UnsupportedOperationException()
    }

    override fun fix(file: TargetFile): String {
        throw UnsupportedOperationException()
    }

    override fun isSupported(file: TargetFile): Boolean {
        throw UnsupportedOperationException()
    }

    override fun toString(): String {
        return type
    }
}
