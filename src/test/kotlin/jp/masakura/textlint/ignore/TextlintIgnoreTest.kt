package jp.masakura.textlint.ignore

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class TextlintIgnoreTest {
    @Test
    fun `empty file`() {
        val target = TextlintIgnore.parse("", "/project1")

        assertFalse(target.isIgnore("/project1/sample.md"))
    }

    @Test
    fun comment() {
        val target = TextlintIgnore.parse("#.txt", "/project1")

        assertFalse(target.isIgnore("/project1/#.txt"))
    }

    @Test
    fun multiline() {
        val target = TextlintIgnore.parse(
            """
                # files
                foo.md
                *.txt # comment
            """.trimIndent(),
            "/project1",
        )

        assertTrue(target.isIgnore("/project1/bar.txt"))
    }
}
