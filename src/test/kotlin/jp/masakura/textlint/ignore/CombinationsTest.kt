package jp.masakura.textlint.ignore

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CombinationsTest {
    @Test
    fun testCombinationsEmpty() {
        assertEquals(emptyList<List<Int>>(), emptyList<Int>().combinations(1))
    }

    @Test
    fun testCombinationsOne() {
        assertEquals(
            listOf(
                listOf(1),
                listOf(2),
                listOf(3),
            ),
            listOf(1, 2, 3).combinations(1),
        )
    }

    @Test
    fun testCombinationsTwo() {
        assertEquals(
            listOf(
                listOf(1, 2),
                listOf(1, 3),
                listOf(2, 3),
            ),
            listOf(1, 2, 3).combinations(2),
        )
    }

    @Test
    fun testCombinationsAll() {
        assertEquals(
            listOf(
                listOf(1, 2, 3),
                listOf(1, 2),
                listOf(1, 3),
                listOf(2, 3),
                listOf(1),
                listOf(2),
                listOf(3),
            ),
            listOf(1, 2, 3).combinationsAll(),
        )
    }

    @Test
    fun testCombinationsAllWithN() {
        assertEquals(
            listOf(
                listOf(1, 2),
                listOf(1, 3),
                listOf(2, 3),
                listOf(1),
                listOf(2),
                listOf(3),
            ),
            listOf(1, 2, 3).combinationsAll(2),
        )
    }
}
