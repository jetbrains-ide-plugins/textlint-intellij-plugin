package jp.masakura.textlint.ignore

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertIterableEquals
import org.junit.jupiter.api.Test

class GlobstarPatternAdjusterTest {
    @Test
    fun `no globstar`() {
        assertEquals(
            emptyList<String>(),
            GlobstarPatternAdjuster.adjust("*/*.txt"),
        )
    }

    @Test
    fun `single globstar pattern`() {
        assertEquals(
            listOf("src/*.txt"),
            GlobstarPatternAdjuster.adjust("src/**/*.txt"),
        )
    }

    @Test
    fun `multiple globstar pattern`() {
        assertIterableEquals(
            listOf(
                "foo/bar/buzz/file.txt",
                "foo/bar/buzz/**/file.txt",
                "foo/bar/**/buzz/file.txt",
                "foo/**/bar/buzz/file.txt",
                "foo/bar/**/buzz/**/file.txt",
                "foo/**/bar/buzz/**/file.txt",
                "foo/**/bar/**/buzz/file.txt",
            ),
            GlobstarPatternAdjuster.adjust("foo/**/bar/**/buzz/**/file.txt"),
        )
    }
}
