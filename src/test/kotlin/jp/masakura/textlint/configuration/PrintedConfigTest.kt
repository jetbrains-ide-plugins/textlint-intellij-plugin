package jp.masakura.textlint.configuration

import jp.masakura.testing.fixture.Fixtures
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.io.ByteArrayInputStream

class PrintedConfigTest {
    @Test
    fun `no processors`() {
        val target = PrintedConfig.parse(
            Fixtures.printedConfigs.EMPTY.toInputStream(),
        )

        val results = ResultMap(
            mapOf(
                "s.txt" to false,
                "s.html" to false,
            ),
        )

        assertEquals(results, results.getIsSupportedResult(target))
    }

    @Test
    fun `support text, all extensions`() {
        val target = PrintedConfig.parse(
            Fixtures.printedConfigs.DEFAULT.toInputStream(),
        )

        val results = ResultMap(
            mapOf(
                "s.txt" to true,
                "s.text" to true,
                "s.textfile" to false,
            ),
        )

        assertEquals(results, results.getIsSupportedResult(target))
    }

    @Test
    fun `support markdown, all extensions`() {
        val target = PrintedConfig.parse(
            Fixtures.printedConfigs.DEFAULT.toInputStream(),
        )

        val results = ResultMap(
            mapOf(
                "s.md" to true,
                "s.markdown" to true,
                "s.mdown" to true,
                "s.mkdn" to true,
                "s.mkd" to true,
                "s.mdwn" to true,
                "s.mkdown" to true,
                "s.ron" to true,
                "s.markdown2" to false,
            ),
        )

        assertEquals(results, results.getIsSupportedResult(target))
    }

    @Test
    fun `support html, all extensions`() {
        val target = PrintedConfig.parse(
            Fixtures.printedConfigs.HTML.toInputStream(),
        )

        val results = ResultMap(
            mapOf(
                "s.htm" to true,
                "s.html" to true,
                "s.xhtml" to false,
            ),
        )

        assertEquals(results, results.getIsSupportedResult(target))
    }

    @Test
    fun `support rst, all extensions`() {
        val target = PrintedConfig.parse(
            Fixtures.printedConfigs.RST.toInputStream(),
        )

        val results = ResultMap(
            mapOf(
                "s.rst" to true,
                "s.rest" to true,
                "s.rt" to false,
            ),
        )

        assertEquals(results, results.getIsSupportedResult(target))
    }

    private fun String.toInputStream() = ByteArrayInputStream(this.toByteArray())
}
