package jp.masakura.intellij.annotation.cache

import com.intellij.openapi.fileTypes.FileType
import jp.masakura.intellij.annotation.target.Target

data class Key(val project: String, val file: String, val fileType: FileType) {
    companion object {
        fun from(target: Target): Key {
            return Key(target.projectPath, target.file.path, target.file.fileType)
        }
    }
}
