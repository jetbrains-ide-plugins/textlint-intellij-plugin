package jp.masakura.intellij.annotation.cache

import jp.masakura.intellij.annotation.issue.Annotations
import jp.masakura.intellij.annotation.target.Target

class Cache {
    private var cache = HashMap<Key, Bucket>()

    fun getOrInvoke(target: Target, annotate: (Target) -> Annotations): Annotations {
        val key = Key.from(target)

        var bucket = cache[key]
        if (bucket == null || bucket.modified(target)) {
            bucket = Bucket.from(target, annotate(target))
            cache[key] = bucket
        }
        return bucket.annotations
    }
}
