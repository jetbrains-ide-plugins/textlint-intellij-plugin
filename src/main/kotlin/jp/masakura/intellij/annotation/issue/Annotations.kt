package jp.masakura.intellij.annotation.issue

import com.intellij.lang.annotation.AnnotationHolder
import jp.masakura.intellij.annotation.label.AnnotatorLabel
import jp.masakura.intellij.annotation.quickfix.QuickFix

data class Annotations(private val annotations: List<Annotation>) : Iterable<Annotation> {
    fun addQuickFixCurrentFile(fixCurrentFile: QuickFix): Annotations {
        return Annotations(annotations.map { it.addQuickFixCurrentFile(fixCurrentFile) })
    }

    fun appendLabel(label: AnnotatorLabel): Annotations {
        return Annotations(annotations.map { it.appendLabel(label) })
    }

    fun appendTo(holder: AnnotationHolder) {
        annotations.forEach { it.appendTo(holder) }
    }

    override fun iterator(): Iterator<Annotation> {
        return annotations.iterator()
    }
}
