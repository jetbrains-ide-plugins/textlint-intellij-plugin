package jp.masakura.intellij.annotation.quickfix

import com.intellij.lang.annotation.AnnotationBuilder
import jp.masakura.intellij.annotation.label.AnnotatorLabel

class QuickFixes(private val items: List<QuickFix>) : Iterable<QuickFix> {
    fun addQuickFixCurrentFile(fixCurrentFile: QuickFix): QuickFixes {
        if (items.any()) {
            return QuickFixes(items + listOf(fixCurrentFile))
        }
        return this
    }

    fun appendLabel(label: AnnotatorLabel): QuickFixes {
        return QuickFixes(items.map { it.appendLabel(label) })
    }

    fun appendTo(builder: AnnotationBuilder) {
        items.forEach {
            builder.withFix(QuickFixAction(it))
        }
    }

    override fun iterator(): Iterator<QuickFix> {
        return items.iterator()
    }
}
