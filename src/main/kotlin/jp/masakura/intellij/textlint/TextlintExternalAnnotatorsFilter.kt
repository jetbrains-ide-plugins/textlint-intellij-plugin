package jp.masakura.intellij.textlint

import com.intellij.lang.ExternalAnnotatorsFilter
import com.intellij.lang.annotation.ExternalAnnotator
import com.intellij.psi.PsiFile
import jp.masakura.intellij.annotation.target.Target
import jp.masakura.intellij.annotation.target.toTargetFile
import jp.masakura.intellij.textlint.configuration.textlintSettings
import jp.masakura.textlint.process.TextlintFactory

class TextlintExternalAnnotatorsFilter(
    private val textlintFactory: TextlintFactory = TextlintFactory.DEFAULT,
) : ExternalAnnotatorsFilter {
    override fun isProhibited(annotator: ExternalAnnotator<*, *>?, file: PsiFile?): Boolean {
        if (annotator !is TextlintExternalAnnotator) return false
        if (file == null) return true

        val target = Target.from(file)

        val textlint = textlintFactory.create(target.projectPath, target.textlintSettings())

        return !textlint.isSupported(file.toTargetFile())
    }
}
