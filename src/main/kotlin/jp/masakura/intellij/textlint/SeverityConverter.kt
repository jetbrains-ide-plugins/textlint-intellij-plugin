package jp.masakura.intellij.textlint

import com.intellij.lang.annotation.HighlightSeverity
import jp.masakura.textlint.report.SeverityLevel

fun SeverityLevel.toHighlightSeverity(): HighlightSeverity {
    return when (this) {
        SeverityLevel.Information -> HighlightSeverity.INFORMATION
        SeverityLevel.Warning -> HighlightSeverity.WARNING
        SeverityLevel.Error -> HighlightSeverity.ERROR
    }
}
