package jp.masakura.intellij.textlint.configuration

import com.intellij.openapi.project.Project
import jp.masakura.intellij.annotation.target.Target
import jp.masakura.textlint.process.settings.TextlintExcludeFiles
import jp.masakura.textlint.process.settings.TextlintSettings
import jp.masakura.textlint.process.settings.TextlintSettingsFilesToScan

interface PluginSettings {
    var excludeFiles: TextlintExcludeFiles
    var printConfig: TextlintSettingsFilesToScan

    fun apply(settings: PluginSettings) {
        apply(settings.toTextlintSettings())
    }

    fun apply(settings: TextlintSettings) {
        excludeFiles = settings.excludeFiles
        printConfig = settings.filesToScan
    }

    fun isModified(original: PluginSettings): Boolean {
        return this.toTextlintSettings() != original.toTextlintSettings()
    }

    fun toTextlintSettings(): TextlintSettings {
        return TextlintSettings(excludeFiles, printConfig)
    }
}

fun Project.pluginSettings(): PluginSettings {
    return PluginSettingsState.byProject(this)
}

fun Target.textlintSettings(): TextlintSettings {
    return this.project.pluginSettings().toTextlintSettings()
}
