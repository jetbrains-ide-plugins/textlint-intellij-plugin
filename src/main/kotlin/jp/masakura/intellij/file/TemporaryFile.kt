package jp.masakura.intellij.file

import com.intellij.openapi.util.io.FileUtil
import java.io.Closeable
import java.io.File
import java.nio.file.Path
import java.util.*
import kotlin.io.path.extension

class TemporaryFile(private val file: File) : Closeable {
    fun load(): String {
        return FileUtil.loadFile(file, Charsets.UTF_8)
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun delete() {
        FileUtil.delete(file)
    }

    val absolutePath: String
        get() = file.absolutePath

    override fun close() {
        delete()
    }

    fun targetFile(): TargetFile {
        return TargetFile(absolutePath, load())
    }

    companion object {
        fun create(originalPath: String, content: String): TemporaryFile {
            val path = Path.of(originalPath)
            val file = FileUtil.createTempFile(UUID.randomUUID().toString(), ".${path.extension}", true)
            FileUtil.writeToFile(file, content, Charsets.UTF_8)
            return TemporaryFile(file)
        }
    }
}
