package jp.masakura.textlint.ignore

import java.util.regex.Pattern

class GlobstarPatternAdjuster {
    companion object {
        fun adjust(pattern: String): List<String> {
            // if pattern is `**/foo/**/bar/**/*.txt`

            // 3 (globstar count)
            val count = pattern.containsCount("**/")

            // * [1, 2, 3] // first and second globstar and third globstar
            // * [1, 2]
            // * [1, 3]
            // * [2, 3]
            // * [1]
            // * [2]
            // * [3]
            val allCombinations =
                (1..count).toList()
                    .combinationsAll()

            // * "foo/bar/buzz/file.txt" // remove `**/` at first and second and third globstar
            // * "foo/bar/buzz/**/file.txt"
            // * "foo/bar/**/buzz/file.txt"
            // * "foo/**/bar/buzz/file.txt"
            // * "foo/bar/**/buzz/**/file.txt"
            // * "foo/**/bar/buzz/**/file.txt"
            // * "foo/**/bar/**/buzz/file.txt"
            return allCombinations.map { pattern.replaceNth("**/", "", it) }
        }
    }
}

private fun String.containsCount(other: String): Int {
    val regex = Regex(Pattern.quote(other))

    return regex.findAll(this).count()
}

private fun String.replaceNth(
    oldValue: String,
    newValue: String,
    nths: List<Int>,
): String {
    val regex = Regex(Pattern.quote(oldValue))

    var i = 0
    return regex.replace(this) {
        i++
        if (nths.contains(i)) {
            newValue
        } else {
            it.value
        }
    }
}
