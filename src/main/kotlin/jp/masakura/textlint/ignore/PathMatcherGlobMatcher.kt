package jp.masakura.textlint.ignore

import java.nio.file.FileSystems
import java.nio.file.Path
import java.nio.file.PathMatcher

class PathMatcherGlobMatcher private constructor(private val matcher: PathMatcher) : GlobMatcher {
    override fun matches(relativePath: Path): Boolean {
        return matcher.matches(relativePath)
    }

    companion object {
        fun create(pattern: String): GlobMatcher {
            return PathMatcherGlobMatcher(FILE_SYSTEM.getPathMatcher("glob:$pattern"))
        }

        private val FILE_SYSTEM = FileSystems.getDefault()
    }
}
