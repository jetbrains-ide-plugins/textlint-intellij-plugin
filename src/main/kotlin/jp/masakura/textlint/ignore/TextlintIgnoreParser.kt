package jp.masakura.textlint.ignore

class TextlintIgnoreParser {
    fun parse(text: String): List<String> {
        return text.lines().mapNotNull { parseLine(it) }
    }

    private fun parseLine(line: String): String? {
        val pattern = "#.*".toRegex().replace(line, "").trim()
        if (pattern.isEmpty()) return null
        return pattern
    }

    companion object {
        val INSTANCE = TextlintIgnoreParser()
    }
}
