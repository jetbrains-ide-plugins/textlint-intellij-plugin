package jp.masakura.textlint.process

import jp.masakura.textlint.command.TextlintCommand
import jp.masakura.textlint.file.FileSystem
import jp.masakura.textlint.process.settings.TextlintSettings

class TextlintFactoryImpl : TextlintFactory {
    override fun create(
        workDirectory: String,
        settings: TextlintSettings,
    ): Textlint {
        return settings.createTextlint(workDirectory, TextlintCommand(workDirectory), FileSystem.INSTANCE)
    }
}
