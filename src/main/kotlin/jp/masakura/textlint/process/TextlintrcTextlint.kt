package jp.masakura.textlint.process

import jp.masakura.intellij.file.TargetFile
import jp.masakura.textlint.file.FileSystem
import jp.masakura.textlint.report.TextlintReport

class TextlintrcTextlint(
    private val textlint: Textlint,
    private val workDirectory: String,
    private val files: FileSystem,
) : Textlint {
    override fun scan(file: TargetFile): TextlintReport {
        return textlint.scan(file)
    }

    override fun fix(file: TargetFile): String {
        return textlint.fix(file)
    }

    override fun isSupported(file: TargetFile): Boolean {
        if (!isExistsTextlintrc()) return false

        return textlint.isSupported(file)
    }

    private fun isExistsTextlintrc(): Boolean {
        return TextlintrcResolver(files).exists(workDirectory)
    }

    override fun toString(): String {
        return "TextlintrcTextlint -> $textlint"
    }
}
