package jp.masakura.textlint.process.settings

import jp.masakura.intellij.file.TargetFile
import jp.masakura.textlint.file.FileSystem
import jp.masakura.textlint.ignore.TextlintIgnore
import jp.masakura.textlint.process.Textlint
import jp.masakura.textlint.report.TextlintReport

class TextlintIgnoreTextlint(
    private val textlint: Textlint,
    private val workDirectory: String,
    private val files: FileSystem,
) : Textlint {
    override fun scan(file: TargetFile): TextlintReport {
        return textlint.scan(file)
    }

    override fun fix(file: TargetFile): String {
        return textlint.fix(file)
    }

    override fun isSupported(file: TargetFile): Boolean {
        val textlintIgnore = TextlintIgnore.load(workDirectory, files)

        if (textlintIgnore.isIgnore(file.path)) return false

        return textlint.isSupported(file)
    }

    override fun toString(): String {
        return "TextlintIgnoreTextlint -> $textlint"
    }
}
