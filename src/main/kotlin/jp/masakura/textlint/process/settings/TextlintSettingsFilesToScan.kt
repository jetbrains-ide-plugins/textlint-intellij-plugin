package jp.masakura.textlint.process.settings

import jp.masakura.textlint.command.TextlintCommand
import jp.masakura.textlint.process.Textlint

enum class TextlintSettingsFilesToScan {
    USE_PRINT_CONFIG {
        override fun wrap(
            textlint: Textlint,
            command: TextlintCommand,
        ): Textlint {
            return PrintConfigFilesToScanFilterTextlint(textlint, command)
        }
    },
    ANY_FILES {
        override fun wrap(
            textlint: Textlint,
            command: TextlintCommand,
        ): Textlint {
            return textlint
        }
    },
    ;

    abstract fun wrap(
        textlint: Textlint,
        command: TextlintCommand,
    ): Textlint
}
