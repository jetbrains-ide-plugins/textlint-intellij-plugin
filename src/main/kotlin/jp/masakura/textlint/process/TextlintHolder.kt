package jp.masakura.textlint.process

import jp.masakura.textlint.process.settings.TextlintSettings

class TextlintHolder(
    private val settings: TextlintSettings,
    val textlint: Textlint,
) {
    fun isSameSetting(settings: TextlintSettings): Boolean {
        return this.settings == settings
    }
}
