package jp.masakura.textlint.process

import jp.masakura.textlint.file.FileSystem
import java.nio.file.Paths

class TextlintrcResolver(private val files: FileSystem) {
    fun exists(workDirectory: String): Boolean {
        return RCS.any { exists(workDirectory, it) }
    }

    private fun exists(workDirectory: String, name: String): Boolean {
        return files.exists(Paths.get(workDirectory, name).toString())
    }

    companion object {
        private val RCS = listOf(
            ".textlintrc",
            ".textlintrc.json",
            ".textlintrc.yaml",
            ".textlintrc.yml",
            ".textlintrc.js",
        )
    }
}
