package jp.masakura.textlint.process

import jp.masakura.textlint.process.settings.TextlintSettings

class CachedTextlintFactory(private val factory: TextlintFactory) : TextlintFactory {
    private val cache = HashMap<String, TextlintHolder>()

    override fun create(
        workDirectory: String,
        settings: TextlintSettings,
    ): Textlint {
        var cachedTextlint = get(workDirectory, settings)
        if (cachedTextlint == null) {
            cachedTextlint = createAndPush(workDirectory, settings)
        }
        return cachedTextlint
    }

    private fun createAndPush(
        workDirectory: String,
        settings: TextlintSettings,
    ): Textlint {
        val holder = TextlintHolder(settings, factory.create(workDirectory, settings))
        cache[workDirectory] = holder
        return holder.textlint
    }

    private fun get(
        workDirectory: String,
        settings: TextlintSettings,
    ): Textlint? {
        val holder = cache[workDirectory]
        if (holder == null || !holder.isSameSetting(settings)) return null
        return holder.textlint
    }
}
