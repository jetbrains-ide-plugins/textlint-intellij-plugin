package jp.masakura.textlint.file

import java.nio.charset.StandardCharsets
import java.nio.file.Path
import kotlin.io.path.exists
import kotlin.io.path.readText

class FileSystemImpl : FileSystem {
    override fun exists(absolutePath: String): Boolean {
        return Path.of(absolutePath).exists()
    }

    override fun readText(absolutePath: String): String {
        return Path.of(absolutePath).readText(StandardCharsets.UTF_8)
    }
}
