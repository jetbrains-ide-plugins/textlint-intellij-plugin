package jp.masakura.textlint.configuration.processor

class Processor(private val plugin: String, private val extensions: Set<String>) {

    fun isSupported(extension: String): Boolean {
        return extensions.contains(extension)
    }

    companion object {
        // see https://github.com/textlint/textlint/blob/master/packages/%40textlint/textlint-plugin-text/src/TextProcessor.ts
        private val TEXT = Processor("@textlint/textlint-plugin-text", setOf("txt", "text"))

        // see https://github.com/textlint/textlint/blob/master/packages/%40textlint/textlint-plugin-markdown/src/MarkdownProcessor.ts
        private val MARKDOWN = Processor("@textlint/textlint-plugin-markdown", setOf("md", "markdown", "mdown", "mkdn", "mkd", "mdwn", "mkdown", "ron"))

        // see https://github.com/textlint/textlint-plugin-html/blob/master/src/HTMLProcessor.ts
        private val HTML = Processor("html", setOf("htm", "html"))

        // see https://github.com/jimo1001/textlint-plugin-rst/blob/master/src/ReSTProcessor.js
        private val RST = Processor("rst", setOf("rst", "rest"))

        fun get(plugins: List<String>): Processors {
            return Processors(plugins.mapNotNull { MAP[it] })
        }

        private val ALL = listOf(
            TEXT,
            MARKDOWN,
            HTML,
            RST,
        )
        private val MAP = ALL.associateBy { it.plugin }
    }
}
