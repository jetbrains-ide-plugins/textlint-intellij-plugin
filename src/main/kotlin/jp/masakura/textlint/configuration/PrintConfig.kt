package jp.masakura.textlint.configuration

import jp.masakura.textlint.command.TextlintCommand

interface PrintConfig {
    fun get(): PrintedConfig

    companion object {
        fun create(command: TextlintCommand): PrintConfig {
            return withCache(PrintConfigImpl(command))
        }

        fun withCache(printConfig: PrintConfig): PrintConfig {
            return CachedPrintConfig(printConfig)
        }
    }
}
