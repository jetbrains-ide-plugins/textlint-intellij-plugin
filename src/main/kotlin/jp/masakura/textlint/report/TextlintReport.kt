package jp.masakura.textlint.report

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.io.InputStream

data class TextlintReport(val issues: List<Issue> = emptyList()) {
    companion object {
        fun from(input: InputStream): TextlintReport {
            val bytes = input.readAllBytes()
            if (bytes.isEmpty()) return TextlintReport()

            val serializer = jacksonObjectMapper()
            return TextlintReport(serializer.readValue<List<Issue>>(bytes))
        }
    }
}
